import SchockenLog

from project.die import SixSidedDiceModel as dice


def translate_dice_number_to_position(number: int) -> str:
    if number == 1:
        return "left"
    if number == 2:
        return "center"
    if number == 3:
        return "right"
    return "ERROR"


"""
Represents the UI-Model for a Player of the Project.
"""


class PlayerModel:

    def __init__(self, name="Unknown"):
        print("\n[START] Player Model Creation")
        self.__name = name
        self.__has_half = False
        self.__dices = {1: dice.SixSidedDice(),
                        2: dice.SixSidedDice(),
                        3: dice.SixSidedDice()}
        # Player starts with zero tokens
        self.__token_count = 0
        # Model starts with zero tries.
        self.__tries_used = 0
        print("\n[FINISH] Player Model Creation")

    def add_token(self, token_count):
        self.__token_count += token_count

    def get_token_count(self):
        return self.__token_count

    def receive_half(self):
        print("\n[HALF RECEIVED EVENT]", "\t(" + self.__name + ")")
        self.__has_half = True

    def reset_half(self):
        self.__has_half = False

    def roll_dice_(self, number):
            self.__dices[number].roll()

    def roll_first_dice(self):
        self.roll_dice_(1)

    def roll_second_dice(self):
        self.roll_dice_(2)

    def roll_third_dice(self):
        self.roll_dice_(3)

    def lock_dice(self, number):
        SchockenLog.log("DICE LOCK EVENT",
                        self.__name + " locked " + translate_dice_number_to_position(number) + " dice.")
        self.__dices[number].lock()

    def lock_first_dice(self):
        self.lock_dice(1)

    def lock_second_dice(self):
        self.lock_dice(2)

    def lock_third_dice(self):
        self.lock_dice(3)

    def unlock_dice(self, number):
        SchockenLog.log("DICE UNLOCK EVENT",
                        self.__name + " unlocked " + translate_dice_number_to_position(number) + " dice.")
        self.__dices[number].unlock()

    def unlock_first_dice(self):
        self.unlock_dice(1)

    def unlock_second_dice(self):
        self.unlock_dice(2)

    def unlock_third_dice(self):
        self.unlock_dice(3)

    def get_eye_count_of_dice(self, number):
        return self.__dices[number].eye_count

    def get_eye_count_of_first_dice(self):
        return self.get_eye_count_of_dice(1)

    def get_eye_count_of_second_dice(self):
        return self.get_eye_count_of_dice(2)

    def get_eye_count_of_third_dice(self):
        return self.get_eye_count_of_dice(3)

    def roll_dice(self):
        self.__tries_used += 1
        self.print_player_rolls()
        self.roll_first_dice()
        self.roll_second_dice()
        self.roll_third_dice()
        self.print_eyes()

    def get_sorted_eyes(self):
        all_eyes = self.get_unsorted_eyes()
        all_eyes.sort(reverse=True)
        return all_eyes

    def get_unsorted_eyes(self):
        all_eyes = []
        for single_dice in self.__dices.values():
            eyes_count = single_dice.eye_count
            all_eyes.append(eyes_count)
        return all_eyes

    def print_eyes(self):
        print("[EYES]", "\t", self.get_unsorted_eyes())

    def print_state(self):
        print("\n[STATUS]", "\t\t", "(" + self.__name + ")")
        print("[TOKENS]", "\t\t", self.__token_count)
        print("[HAS HALF]", "\t\t", self.__has_half)
        print("[TRIES USED]", "\t", self.__tries_used)
        print("[CURRENT EYES]", "\t", self.get_sorted_eyes())

    def print_player_rolls(self):
        print("\n[ROLL EVENT]  (" + self.__name + ")")

    def remove_token(self, amount):
        self.__token_count -= amount

    def reset_tokens(self):
        self.__token_count = 0

    def get_name(self):
        return self.__name

    def has_half(self):
        return self.__has_half

    def get_tries_used(self):
        return self.__tries_used

    def reset_tries_used(self):
        self.__tries_used = 0

    def reset_dice(self):
        for die in self.__dices.values():
            die.reset()
