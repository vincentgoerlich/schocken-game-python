from tkinter import PanedWindow, Button, Label

from DiceButton import DiceButton
from IconCache import IconCache

"""
View-class presenting a player's hud. Three dices, a roll and end turn button (if its a human player).
"""


class PlayerDiceView(PanedWindow):

    def __init__(self, root, icon_cache: IconCache, is_human=False):
        super(PlayerDiceView, self).__init__(root)
        # Components
        self.__icon_cache = icon_cache
        self.__back_ground_panel = Label(self, image=icon_cache.dice_background)
        self.__back_ground_panel.pack(expand=True, fill="both")
        self.__left_dice_button = DiceButton(self.__back_ground_panel, self.__icon_cache)
        self.__center_dice_button = DiceButton(self.__back_ground_panel, self.__icon_cache)
        self.__right_dice_button = DiceButton(self.__back_ground_panel, self.__icon_cache)
        self.__roll_button = Button(self.__back_ground_panel)
        self.__end_turn_button = Button(self.__back_ground_panel, text="End Turn", state="normal", font=['Arial', 14, 'bold'])
        # Layouting
        self.__left_dice_button.pack(side="left", expand=True, padx=5)
        self.__end_turn_button.pack(side="right", expand=True, padx=5)
        self.__roll_button.pack(side="right", expand=True, padx=20)
        self.__right_dice_button.pack(side="right", expand=True, padx=5)
        self.__center_dice_button.pack(side="right", expand=True, padx=5)
        # Default Icons
        self.__roll_button.configure(image=self.__icon_cache.roll_image)
        self.update_left_dice_button_icon(0)
        self.update_center_dice_button_icon(0)
        self.update_right_dice_button_icon(0)

    def update_left_dice_button_icon(self, eye_amount):
        self.__left_dice_button.set_eyes_and_update_icon(eye_amount)

    def update_center_dice_button_icon(self, eye_amount):
        self.__center_dice_button.set_eyes_and_update_icon(eye_amount)

    def update_right_dice_button_icon(self, eye_amount):
        self.__right_dice_button.set_eyes_and_update_icon(eye_amount)

    def lock_and_disable_dices(self):
        self.__left_dice_button.lock_and_update_icon()
        self.__left_dice_button.configure(state="disabled")
        self.__center_dice_button.lock_and_update_icon()
        self.__center_dice_button.configure(state="disabled")
        self.__right_dice_button.lock_and_update_icon()
        self.__right_dice_button.configure(state="disabled")

    def unlock_and_enable_dices(self):
        self.__left_dice_button.unlock_and_update_icon()
        self.__left_dice_button.configure(state="normal")
        self.__center_dice_button.unlock_and_update_icon()
        self.__center_dice_button.configure(state="normal")
        self.__right_dice_button.unlock_and_update_icon()
        self.__right_dice_button.configure(state="normal")

    def reset(self):
        self.__left_dice_button.set_eyes_and_update_icon(0)
        self.__center_dice_button.set_eyes_and_update_icon(0)
        self.__right_dice_button.set_eyes_and_update_icon(0)
        self.unlock_and_enable_dices()
        self.enable_roll_button()

    def disable_roll_button(self):
        self.__roll_button.configure(state="disabled")

    def enable_roll_button(self):
        self.__roll_button.configure(state="normal")

    def enable_round_end_button(self):
        self.__end_turn_button.configure(state="normal")

    def disable_round_end_button(self):
        self.__end_turn_button.configure(state="disabled")

    def get_roll_button(self) -> Button:
        return self.__roll_button

    def get_right_dice_button(self) -> DiceButton:
        return self.__right_dice_button

    def get_left_dice_button(self) -> DiceButton:
        return self.__left_dice_button

    def get_center_dice_button(self) -> DiceButton:
        return self.__center_dice_button

    def get_end_turn_button(self) -> Button:
        return self.__end_turn_button

