from PlayerModel import PlayerModel

"""
Data Container being initialized in the RoundEndInterpreter
"""
class RoundEndSummary:

    def __init__(self, winner: PlayerModel, loser: PlayerModel, tokens_for_loser: int):
        self.winner = winner
        self.loser = loser
        self.tokens_for_loser = tokens_for_loser
        self.print_result()

    def print_result(self):
        print("\n[ROUND END SUMMARY]")
        print("[WINNER]", "\t\t", self.winner.get_name())
        print("[LOSER]", "\t\t", self.loser.get_name())
        print("[TOKENS]", "\t\t", str(self.tokens_for_loser))
