"""
The GameConfigurationContext defines configurable rules
on how Schocken and can be modified.
"""
class GameConfigurationContext:

    def __init__(self,
                 in_game_token_count=11,
                 maximum_tries_per_turn=3,
                 play_two_halves=True,
                 beginning_player_dictates_try_count=True,
                 same_try_count_same_result_results_in_loss_for_follow_player=True,
                 less_try_count_same_result_results_in_win_for_follow_player=True,
                 loser_begins=True,
                 shock_out_gives_all_tokens_to_opponent=True
                 ):

        self.__in_game_token_count = in_game_token_count
        self.__play_two_halves = play_two_halves
        self.__maximum_tries_per_round = maximum_tries_per_turn
        self.__beginning_player_dictates_try_count = beginning_player_dictates_try_count
        self.__same_try_count_same_result_results_in_loss_for_follow_player = \
            same_try_count_same_result_results_in_loss_for_follow_player
        self.__less_try_count_same_result_results_in_win_for_follow_player = \
            less_try_count_same_result_results_in_win_for_follow_player
        self.__loser_begins = loser_begins
        self.__shock_out_gives_all_tokens_to_opponent = shock_out_gives_all_tokens_to_opponent

    def less_try_count_same_result_results_in_win_for_follow_player(self):
        return self.__less_try_count_same_result_results_in_win_for_follow_player




