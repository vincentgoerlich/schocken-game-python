from tkinter import PanedWindow

from IconCache import IconCache
from PlayerDiceView import PlayerDiceView
from PlayerStatsView import PlayerStatsView
from TokenView import TokenView

"""
Represents the UI-View for a Player of the Project.
"""
class PlayerView(PanedWindow):

    def __init__(self, root, dice_icon_cache: IconCache, name: str = "Computer", mirrored=True, is_human=False):
        super(PlayerView, self).__init__(root, orient="horizontal", background="black")
        # Load Icons into Cache.
        self.dice_icon_cache = dice_icon_cache
        # Init Components
        self.player_dice_view = PlayerDiceView(self, icon_cache=dice_icon_cache, is_human=is_human)
        self.player_stats_view = PlayerStatsView(self, name)
        self.token_view = TokenView(self, dice_icon_cache, orient="horizontal", initial_token_count=0,
                                    is_mirrored=is_human)
        # Layouting
        self.layout_components(mirrored)

    def layout_components(self, mirrored) -> None:
        if mirrored:
            self.player_stats_view.pack(side="top", expand=True, fill="both")
            self.player_dice_view.pack(side="top", expand=True, fill="both", pady=5)
            self.token_view.pack(side="top", expand=True, pady=5)
        else:
            self.player_stats_view.pack(side="bottom", expand=True, fill="both")
            self.player_dice_view.pack(side="bottom", expand=True, fill="both", pady=5)
            self.token_view.pack(side="top", expand=True, pady=5)

    def update_left_dice_button_icon(self, eye_amount) -> None:
        self.player_dice_view.get_left_dice_button().set_eyes_and_update_icon(eye_amount)

    def update_center_dice_button_icon(self, eye_amount) -> None:
        self.player_dice_view.get_center_dice_button().set_eyes_and_update_icon(eye_amount)

    def update_right_dice_button_icon(self, eye_amount) -> None:
        self.player_dice_view.get_right_dice_button().set_eyes_and_update_icon(eye_amount)

    def on_roll_button_clicked(self, command) -> None:
        self.player_dice_view.get_roll_button().configure(command=command)

    def disable_and_lock_all_dices(self) -> None:
        self.player_dice_view.lock_and_disable_dices()

    def on_end_turn_button_clicked(self, command):
        self.player_dice_view.get_end_turn_button().configure(command=command)

    def disable_end_turn_button(self):
        self.player_dice_view.disable_round_end_button()

    def enable_end_turn_button(self):
        self.player_dice_view.enable_round_end_button()

    def disable_roll_button(self):
        self.player_dice_view.disable_roll_button()

    def enable_roll_button(self):
        self.player_dice_view.enable_roll_button()

    def add_token(self):
        self.token_view.add_token()

    def remove_token(self):
        self.token_view.remove_token()

    def enable(self):
        self.player_dice_view.reset()

    def disable(self):
        self.disable_end_turn_button()
        self.disable_roll_button()
        self.disable_and_lock_all_dices()

    def reset_tokens(self):
        self.token_view.reset_tokens()

