from GameConfigurationContext import GameConfigurationContext
from tkinter import simpledialog
from tkinter import messagebox
from GameView import GameView
from IconCache import IconCache
from PlayerController import PlayerController
from RoundEndInterpreter import RoundEndInterpreter
from RoundEndSummary import RoundEndSummary

"""
Controller-class orchestrating both computer's model & view as well as
the available tokens.
"""


class GameController:

    def __init__(self, root, icon_cache: IconCache, game_configuration: GameConfigurationContext,
                 player_controller_map: dict[str, PlayerController]):
        self.__root = root
        self.__game_configuration = game_configuration
        self.__play_controller_map = player_controller_map
        player_ctrls = self.__play_controller_map.values()
        player_models = []
        for player_ctrl in player_ctrls:
            player_models.append(player_ctrl.get_player_model())
        self.self_game_view = GameView(root, icon_cache, player_controller_map)
        self.__round_end_handler = RoundEndInterpreter(player_models)
        if len(self.__play_controller_map) > 0:
            self.get_player_one_controller().wire_end_turn_button(command=self.on_player_one_lock_in_results)
            self.get_player_two_controller().wire_end_turn_button(command=self.on_player_two_lock_in_results)
        self.__round_count = 0

    def start_game(self) -> None:
        """
        The start of the game defines the first player as the opener by default.
        """
        self.player_one_turn()

    def round_end(self) -> None:
        """
        Decides on winner/loser of the round and transfers the corresponding tokens to
        the loser.
        """
        round_summary = self.__round_end_handler.summarize_round()
        # Rule says:
        # If global tokens are left, the losing player receives
        # either the calculated amount of tokens for the winning players dice eyes
        # or the residual amount of global tokens if the calculated amount
        # exceeds them.
        if self.self_game_view.has_global_tokens_left():
            global_tokens_left = self.self_game_view.token_count_left()
            if global_tokens_left < round_summary.tokens_for_loser:
                tokens_transferred_to_loser = global_tokens_left
            else:
                tokens_transferred_to_loser = round_summary.tokens_for_loser
            self.self_game_view.remove_global_tokens(tokens_transferred_to_loser)
            if round_summary.loser == self.get_player_one_controller().get_player_model():
                self.get_player_one_controller().add_token(tokens_transferred_to_loser)
            else:
                self.get_player_two_controller().add_token(tokens_transferred_to_loser)
        # Rule says:
        # If global tokens are off the table, the tokens are transferred between participants.
        else:
            if round_summary.winner.get_token_count() < round_summary.tokens_for_loser:
                tokens_transferred_to_loser = round_summary.winner.get_token_count()
            else:
                tokens_transferred_to_loser = round_summary.tokens_for_loser
            if round_summary.loser == self.get_player_one_controller().get_player_model():
                self.get_player_one_controller().add_token(tokens_transferred_to_loser)
                self.get_player_two_controller().remove_token(tokens_transferred_to_loser)
            else:
                self.get_player_two_controller().add_token(tokens_transferred_to_loser)
                self.get_player_one_controller().remove_token(tokens_transferred_to_loser)

        self.get_player_one_controller().get_player_model().print_state()
        self.get_player_two_controller().get_player_model().print_state()
        # Check whether one of the participants has 11 Tokens, meaning, they receive the half.
        # If they already have a half and acquired another 11 Tokens, it means, that
        #
        if self.get_player_one_controller().get_player_model().get_token_count() == 11:
            if self.get_player_one_controller().get_player_model().has_half():
                self.second_half_event(self.get_player_two_controller().get_player_model().get_name())
            else:
                self.get_player_one_controller().get_player_model().receive_half()
                self.first_half_event(self.get_player_one_controller().get_player_model().get_name())

        elif self.get_player_two_controller().get_player_model().get_token_count() == 11:
            if self.get_player_two_controller().has_half():
                self.second_half_event(self.get_player_one_controller().get_player_model().get_name())
            else:
                self.get_player_two_controller().get_player_model().receive_half()
                self.first_half_event(self.get_player_two_controller().get_player_model().get_name())

        self.get_player_one_controller().reset()
        self.get_player_two_controller().reset()
        self.on_new_round(round_summary)

    def on_new_round(self, round_summary: RoundEndSummary) -> None:
        # Enable the losing player to start off the new round
        # Disable the winning player.
        if round_summary.loser == self.get_player_one_controller().get_player_model():
            self.get_player_one_controller().enable_view()
            self.get_player_two_controller().disable_view()
        else:
            self.get_player_two_controller().enable_view()
            self.get_player_one_controller().disable_view()
        # Both players are reset
        self.get_player_one_controller().set_has_locked_in(False)
        self.get_player_two_controller().set_has_locked_in(False)
        self.get_player_one_controller().unlock_all_dice();
        self.get_player_two_controller().unlock_all_dice();
        self.get_player_one_controller().reset_tries_used()
        self.get_player_two_controller().reset_tries_used()

    def on_player_one_lock_in_results(self) -> None:
        self.get_player_one_controller().lock_in_results()
        if self.get_player_two_controller().has_locked_in_results():
            self.round_end()
        else:
            self.get_player_two_controller().enable_view()

    def on_player_two_lock_in_results(self) -> None:
        self.get_player_two_controller().lock_in_results()
        if self.get_player_one_controller().has_locked_in_results():
            self.round_end()
        else:
            self.get_player_one_controller().enable_view()

    def player_one_turn(self) -> None:
        self.disable_player_two_view()
        self.get_player_one_controller().on_new_round()
        pass

    def is_first_round(self) -> bool:
        return self.__round_count == 0

    def get_player_one_controller(self) -> PlayerController:
        return self.__play_controller_map["Player One"]

    def get_player_two_controller(self) -> PlayerController:
        return self.__play_controller_map["Player Two"]

    def new_round(self) -> None:
        self.get_player_one_controller().reset()
        self.get_player_two_controller().reset()
        self.start_game()

    def first_half_event(self, name: str = "bla") -> None:
        messagebox.showinfo("First Half Event!", message=name + " received first half!")
        self.get_player_two_controller().reset_tokens()
        self.get_player_one_controller().reset_tokens()
        self.self_game_view.token_view.reset_tokens(11)

    def disable_player_one_view(self) -> None:
        self.get_player_one_controller().disable_view()

    def disable_player_two_view(self) -> None:
        self.get_player_two_controller().disable_view()

    def second_half_event(self, winning_name: str) -> None:
        if messagebox.askyesno("Play Again?", message=winning_name + " has won!"):
            self.get_player_one_controller().reset()
            self.get_player_one_controller().get_player_model().reset_half()
            self.get_player_two_controller().reset()
            self.get_player_two_controller().get_player_model().reset_half()
            self.self_game_view.token_view.reset_tokens(11)
        else:
            exit()

