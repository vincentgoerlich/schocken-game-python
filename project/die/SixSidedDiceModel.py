import random


# Repräsentiert einen sechsseitigen Spielwürfel
class SixSidedDice:

    def __init__(self):
        self.locked = False
        self.eye_count = 0

    # Liefert eine zufällige Zahl zwischen 1 und 6 zurück.
    def roll(self):
        if self.locked is False:
            self.eye_count = random.randint(1, 6)

    def lock(self):
        self.locked = True

    def unlock(self):
        self.locked = False

    def reset(self):
        self.eye_count = 0
