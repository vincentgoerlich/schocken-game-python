from tkinter import PanedWindow, Label

from TriesView import TriesView

"""
Represents the UI-VIew for the Player Statistics.
"""
class PlayerStatsView(PanedWindow):

    def __init__(self, root, name: str = "Computer"):
        super(PlayerStatsView, self).__init__(root)
        self.name_label = Label(self, text=name, font=['Arial', 25, 'bold'])
        self.tries_view = TriesView(self)
        self.layout()

    def layout(self):
        self.name_label.pack(side="left", expand=True, fill="both")
        self.tries_view.pack(side="right", expand=True, fill="both")

    def update_tries_used(self,tries: int):
        self.tries_view.update_tries_used(tries)
