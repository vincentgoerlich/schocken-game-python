from GameConfigurationContext import GameConfigurationContext
from PlayerModel import PlayerModel


def is_shock(eyes: list[int]) -> bool:
    """
    Determines whether the input dice eyes are considered a "shock", i.e., they contain two or more ones
    :param eyes: the values of the three dice.
    :return: true if at least two eyes are ones, else false.
    """
    if len([eye for eye in eyes if eye == 1]) > 1:
        return True
    else:
        return False


def is_shock_out(eyes: list[int]) -> bool:
    """
    Determines whether the input dice eyes are considered a "shock-out", i.e., they consist only of ones.
    :param eyes: the values of the three dice.
    :return: true if tripple-1, else false.
    """
    if len([eye for eye in eyes if eye == 1]) == 3:
        return True
    else:
        return False


def is_triplet(eyes: list[int]) -> bool:
    """
    Determines whether the input dice eyes are considered a "triplet", i.e.,
    they consist only of three equal ints being NOT one
    :param eyes: the values of the three dice.
    :return: true if all eye values have the same value.
    """
    if not is_shock_out(eyes):
        first_eye = eyes[0]
        if len([eye for eye in eyes if eye == first_eye]) == 3:
            return True
    else:
        return False


def is_street(eyes: list[int]) -> bool:
    """
    Determines whether the input dice eyes are considered a "street", i.e.,
       they consist of 3 consecutive numbers, i.e. 6-5-4, 5-4-3 etc.
    :param eyes: the values of the three eyes.
    :return: true if three consecutive values, false if not.
    """
    if eyes[0] == eyes[1] + 1 and eyes[1] == eyes[2] + 1:
        return True
    else:
        return False


def has_shock(player: PlayerModel) -> bool:
    # Determines if the player rolled a shock.
    return is_shock(player.get_sorted_eyes())


def has_shock_out(player: PlayerModel) -> bool:
    # Determines if the player rolled a shock-out.
    return is_shock_out(player.get_sorted_eyes())


def has_triplet(player: PlayerModel) -> bool:
    # Determines if the player rolled a triplet.
    return is_triplet(player.get_sorted_eyes())


def has_street(player: PlayerModel) -> bool:
    # Determines if the player rolled a street.
    return is_street(player.get_sorted_eyes())


def get_winner(opening_player: PlayerModel, responding_player: PlayerModel,
               game_configuration_context: GameConfigurationContext) -> PlayerModel:
    # Determines and returns the round winning player_model.
    # First has a combo hierarchically higher than second: opening_player is winner
    if has_shock_out(opening_player) and not has_shock_out(responding_player) \
            or has_shock(opening_player) and not has_shock(responding_player) \
            or has_triplet(opening_player) and not has_triplet(responding_player) \
            or has_street(opening_player) and not has_street(responding_player):
        return opening_player
    # Second has a combo hierarchically higher than first: responding_player is winner
    elif has_shock_out(responding_player) and not has_shock_out(opening_player) \
            or has_shock(responding_player) and not has_shock(opening_player) \
            or has_triplet(responding_player) and not has_triplet(opening_player) \
            or has_street(responding_player) and not has_street(opening_player):
        return responding_player
    # They have a combo hierarchically on the same level: compare eyes.
    first_eyes = opening_player.get_sorted_eyes()
    second_eyes = responding_player.get_sorted_eyes()
    for eye_count in range(0, 3):
        if first_eyes[eye_count] == second_eyes[eye_count]:
            continue
        elif first_eyes[eye_count] > second_eyes[eye_count]:
            return opening_player
        else:
            return responding_player
    # Coming here means they have identical eyes: check rule set and curate the winner
    # First configurable aspect is to compare the try count.
    if game_configuration_context.less_try_count_same_result_results_in_win_for_follow_player():
        tries_opening_player = opening_player.get_tries_used()
        tries_responding_player = responding_player.get_tries_used()
        # The rule states that if both competitors have the same eyes
        # and the opening_player has used more tries than the responding_player
        # the responding_player wins.
        if tries_opening_player > tries_responding_player:
            return responding_player
        # if both competitors have the same eyes
        # and the opening_player has used the same amount of tries or less than the responding_player
        # the opening_player wins.
        elif tries_opening_player <= tries_responding_player:
            return opening_player
    # Tries are not taken into equation, eyes are identical, by default opening_player wins.
    return opening_player


class DiceInterpreter:
    pass
