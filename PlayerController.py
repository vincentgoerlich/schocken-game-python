from PlayerModel import PlayerModel
from PlayerView import PlayerView

"""
Controller-class orchestrating a PlayerModel and a PlayerView as a well as the player's TokenView.
"""


class PlayerController:

    def __init__(self, root, image_cache, player_model: PlayerModel, mirrored=True, is_human=False):
        self.__dice_icon_cache = image_cache
        self.__player_model = player_model
        self.__player_view = PlayerView(root, self.__dice_icon_cache, self.__player_model.get_name(), mirrored=mirrored,
                                        is_human=is_human)
        # wireButtonToRoll
        self.__player_view.on_roll_button_clicked(command=self.roll)
        self.__tries_left = 0
        self.__has_locked_in_results = False
        self.reset_tries()

    def reset_tries(self) -> None:
        self.set_tries(3)
        self.__player_view.disable_end_turn_button()

    def decrement_tries(self) -> None:
        self.set_tries(self.__tries_left - 1)

    def roll(self) -> None:
        if self.__tries_left > 0:
            print("Works")
            self.update_player_model()
            self.__player_model.roll_dice()
            self.decrement_tries()
            self.__player_view.enable_end_turn_button()
            self.update_player_view()

    def lock_in_results(self) -> None:
        print("\n[TURN OVER EVENT]")
        print("[" + self.__player_model.get_name() + "]")
        self.__has_locked_in_results = True
        self.disable_view()

    def reset(self) -> None:
        self.__has_locked_in_results = False
        self.reset_tries()
        self.get_player_model().reset_dice()
        self.on_new_round()
        self.enable_view()

    def reset_tokens(self) -> None:
        self.__player_model.reset_tokens()
        self.__player_view.reset_tokens()

    def wire_end_turn_button(self, command) -> None:
        self.__player_view.on_end_turn_button_clicked(command)

    def set_tries(self, tries_given) -> None:
        self.__tries_left = tries_given

    def update_player_model(self) -> None:
        if self.__player_view.player_dice_view.get_left_dice_button().is_locked():
            self.__player_model.lock_first_dice()
        if self.__player_view.player_dice_view.get_center_dice_button().is_locked():
            self.__player_model.lock_second_dice()
        if self.__player_view.player_dice_view.get_right_dice_button().is_locked():
            self.__player_model.lock_third_dice()

    def update_player_view(self) -> None:
        self.__player_view.player_stats_view.update_tries_used(self.__player_model.get_tries_used())
        unsorted_eyes = self.__player_model.get_unsorted_eyes()
        for eye in range(0, 3):
            eye_count = unsorted_eyes[eye]
            if eye == 0:
                self.__player_view.update_left_dice_button_icon(eye_count)
            if eye == 1:
                self.__player_view.update_center_dice_button_icon(eye_count)
            if eye == 2:
                self.__player_view.update_right_dice_button_icon(eye_count)

    def add_token(self, count: int = 1) -> None:
        self.__player_model.add_token(count)
        for amount in range(0, count):
            self.__player_view.add_token()

    def remove_token(self, count: int = 1) -> None:
        self.__player_model.remove_token(count)
        for amount in range(0, count):
            self.__player_view.remove_token()

    def has_tries_left(self) -> bool:
        return self.__tries_left > 0

    def has_half(self) -> bool:
        return self.__player_model.has_half()

    def on_new_round(self) -> None:
        self.__player_model.reset_tries_used()
        self.update_player_view()

    def get_player_model(self) -> PlayerModel:
        return self.__player_model

    def get_player_view(self) -> PlayerView:
        return self.__player_view

    def has_locked_in_results(self) -> bool:
        return self.__has_locked_in_results

    def disable_view(self) -> None:
        self.__player_view.disable()

    def enable_view(self) -> None:
        self.__player_view.enable()

    def set_has_locked_in(self, has_locked_in: bool) -> None:
        self.__has_locked_in_results = has_locked_in

    def reset_tries_used(self) -> None:
        self.__player_model.reset_tries_used()
        self.reset_tries()
        self.update_player_view()

    def unlock_all_dice(self):
        self.get_player_model().unlock_first_dice()
        self.get_player_model().unlock_second_dice()
        self.get_player_model().unlock_third_dice()
