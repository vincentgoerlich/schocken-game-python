import DiceInterpreter
import TokenUtil
from GameConfigurationContext import GameConfigurationContext
from PlayerModel import PlayerModel
from RoundEndSummary import RoundEndSummary

"""
Class to be called when both players have locked in their results.
This Class is dedicated to determine the loser, the winner and the token count distributed to the loser.
Uses the DiceInterpreter for comparing results.
"""
class RoundEndInterpreter:

    def __init__(self, player_models: list[PlayerModel]):
        self.rule_set = GameConfigurationContext()
        self.participant_list = player_models

    def summarize_round(self) -> RoundEndSummary:
        """
        Decides upon winner,loser and the token count for the loser.
        :return: collected data as a RoundEndSummary Object.
        """
        winner = self.get_winner(self.participant_list)
        loser = self.get_loser(self.participant_list)
        tokens_for_loser = TokenUtil.translate_eyes_to_tokens(winner)
        return RoundEndSummary(winner, loser, tokens_for_loser)

    def get_loser(self, participants: list[PlayerModel]) -> PlayerModel:
        """
        Extracts the overall loser from all participants.
        :param participants: all participants
        :return: losing PlayerModel
        """
        not_loser_list = []
        index = 0
        while len(not_loser_list) != len(participants) - 1:
            not_loser = DiceInterpreter.get_winner(participants[index], participants[index + 1], self.rule_set)
            not_loser_list.append(not_loser)
            index = index + 1
        for participant in participants:
            if participant in not_loser_list:
                continue
            else:
                return participant

    def get_winner(self, participants: list[PlayerModel]) -> PlayerModel:
        """
        Extracts the overall winner of all participants
        :param participants: all participant
        :return: winning PlayerModel
        """
        winner = participants[0]
        other_participants = [participant for participant in participants if participant != winner]
        for participant in other_participants:
            winner = DiceInterpreter.get_winner(winner, participant, self.rule_set)
        return winner
