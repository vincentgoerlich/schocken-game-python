from tkinter import PanedWindow, Label


class TriesView(PanedWindow):

    def __init__(self, root):
        super(TriesView, self).__init__(root)
        self.try_counter_label = Label(self,font=['Arial',14,'bold'])
        self.max_tries = 3
        self.tries_used = 0
        self.layout()

    def update_try_counter_label(self):
        self.try_counter_label.configure(text=self.tries_display_str())

    def alter_max_tries(self, max_tries: int = 0):
        self.max_tries = max_tries
        self.update_try_counter_label()

    def update_tries_used(self, tries: int):
        self.tries_used = tries
        self.update_try_counter_label()

    def reset_tries_used(self):
        self.tries_used = 0
        self.update_try_counter_label()

    def tries_display_str(self) -> str:
        return "Tries (" + str(self.tries_used) + "/" + str(self.max_tries) + ")"

    def layout(self):
        self.try_counter_label.pack(fill="both", expand=True)
        self.update_try_counter_label()
