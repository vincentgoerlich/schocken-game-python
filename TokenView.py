from tkinter import PanedWindow, Label

from IconCache import IconCache


class TokenView(PanedWindow):

    def __init__(self, root, icon_cache: IconCache, orient="vertical", initial_token_count=11,
                 is_mirrored: bool = False):
        super(TokenView, self).__init__(root, background="black", orient=orient)
        self.icon_cache = icon_cache
        self.is_mirrored = is_mirrored
        self.tokens_to_display = initial_token_count
        self.update_view()

    def create_token(self):
        return Label(self, text="token", image=self.icon_cache.get_token_icon(), background="red")

    def update_view(self):
        self.clear_view()
        self.create_and_layout_tokens()

    def clear_view(self):
        for component in self.winfo_children():
            component.destroy()

    def create_and_layout_tokens(self):
        for token in range(0, self.tokens_to_display):
            self.create_token().pack(side="left")

    def add_token(self, amount: int = 1):
        for token in range(0, amount):
            self.tokens_to_display += 1
        self.update_view()

    def remove_token(self, amount: int = 1):
        for token in range(0, amount):
            self.tokens_to_display -= 1
        self.update_view()

    def reset_tokens(self, amount: int = 0):
        self.tokens_to_display = amount
        self.update_view()
