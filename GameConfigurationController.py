from GameConfigurationContext import GameConfigurationContext
from GameConfigurationView import GameConfigurationView
from PlayerModel import PlayerModel

"""
The GameConfigurationController orchestrates the interaction of the UI-Inputs of the Start Window and
the GameConfigurationContext object.
"""


class GameConfigurationController:

    def __init__(self):
        print("\n[START] Game Configuration")
        self.game_configuration = GameConfigurationContext()
        self.__game_configuration_view = GameConfigurationView()
        self.player_model = None
        self.computer_model = None
        self.__game_configuration_view.on_start_button_clicked(command=self.start_button_clicked)

    def collect_data_and_generate_player_models(self) -> None:
        print("\n[FINISH] Game Configuration")
        player_one_name = self.__game_configuration_view.get_first_player_name_input()
        player_two_name = self.__game_configuration_view.get_second_player_name_input()
        self.player_model = PlayerModel(player_one_name)
        self.computer_model = PlayerModel(player_two_name)

    def start_button_clicked(self, command) -> None:
        self.__game_configuration_view.on_start_button_clicked(command)

    def get_view(self) -> GameConfigurationView:
        return self.__game_configuration_view
