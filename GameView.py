from tkinter import PanedWindow

from PlayerController import PlayerController
from TokenView import TokenView
from IconCache import IconCache


class GameView(PanedWindow):

    def __init__(self, root, icon_cache: IconCache, participant_controller_map: dict[str, PlayerController]):
        super(GameView, self).__init__(root)
        self.token_view = TokenView(self, icon_cache)
        self.participant_controller_map = participant_controller_map
        if len(self.participant_controller_map) != 0:
            self.layout()

    def layout(self) -> None:
        self.participant_controller_map["Player One"].get_player_view().pack(side="bottom", expand=True, fill="both")
        self.participant_controller_map["Player Two"].get_player_view().pack(side="top", expand=True, fill="both")
        self.token_view.pack(expand=True, fill="both")

    def has_global_tokens_left(self) -> bool:
        return self.token_count_left() != 0

    def token_count_left(self) -> int:
        return self.token_view.tokens_to_display

    def add_token_to_player_one(self, amount: int) -> None:
        for token in range(0, amount):
            self.participant_controller_map["Player One"].get_player_view().add_token()

    def add_token_to_player_two(self, amount: int) -> None:
        for token in range(0, amount):
            self.participant_controller_map["Player Two"].get_player_view().add_token()

    def remove_global_tokens(self, amount: int) -> None:
        self.token_view.remove_token(amount=amount)
