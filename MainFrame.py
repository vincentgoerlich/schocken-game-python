from tkinter import *

from GameConfigurationContext import GameConfigurationContext
from GameConfigurationController import GameConfigurationController
from GameController import GameController
from PlayerController import PlayerController
from IconCache import IconCache

"""
Represents the UI Mainframe, i.e. the ultimate root to all other components.
It is to start with the GameConfiguration to allow setting the two players and to configure
the game's ruleset. The inbuilt start button then again allows for starting the actual game.
"""


class SchockenMainFrame(Tk):

    def __init__(self):
        super(SchockenMainFrame, self).__init__()
        self.title("Schocken")
        self.minsize(500, 500)
        self.maxsize(500, 500)
        self.__image_cache = IconCache()
        self.__player_model_map = {}
        self.__player_controller_map = {}
        self.__game_controller = GameController(self, self.__image_cache, GameConfigurationContext(),
                                                self.__player_controller_map)
        self.__game_configuration_controller = GameConfigurationController()
        self.__game_configuration_controller.get_view().pack(expand=True, fill=BOTH)
        self.wire_start_button()

    def wire_start_button(self) -> None:
        self.__game_configuration_controller.start_button_clicked(command=self.start_button_events)

    def start_button_events(self) -> None:
        self.collect_data_from_configuration()
        self.clear_view()
        self.build_player_controllers_from_models()
        self.build_game_controller_from_player_controllers()
        self.layout_game()
        self.__game_controller.start_game()

    def collect_data_from_configuration(self):
        self.__game_configuration_controller.collect_data_and_generate_player_models()
        self.__player_model_map["Player One"] = self.__game_configuration_controller.player_model
        self.__player_model_map["Player Two"] = self.__game_configuration_controller.computer_model

    def clear_view(self) -> None:
        # Destroys all child components on the Mainframe.
        for child in self.winfo_children():
            child.destroy()

    def build_player_controllers_from_models(self) -> None:
        for player_number, player_model in self.__player_model_map.items():
            if player_number == "Player One":
                self.__player_controller_map[player_number] = PlayerController(self,
                                                                               image_cache=self.__image_cache,
                                                                               player_model=player_model,
                                                                               mirrored=False,
                                                                               is_human=True)
            else:
                self.__player_controller_map[player_number] = PlayerController(self, self.__image_cache, player_model)

    def build_game_controller_from_player_controllers(self) -> None:
        self.__game_controller = GameController(self, self.__image_cache,
                                                self.__game_configuration_controller.game_configuration,
                                                self.__player_controller_map)

    def layout_game(self) -> None:
        self.__game_controller.self_game_view.pack(expand=True, fill="both")
