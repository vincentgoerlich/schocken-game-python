from tkinter import PanedWindow, Label, Entry, Button


class GameConfigurationView(PanedWindow):

    def __init__(self):
        super(GameConfigurationView, self).__init__()
        # Components
        self.__header_label = Label(self, text="Schocken - Das Kneipentrinkspiel", font=['Arial', 20, 'bold'])
        self.__player_one_label = Label(self, text="Spieler 1:", font=['Arial', 14, 'bold'])
        self.__player_one_name_entry = Entry(self)
        self.__player_two_label = Label(self, text="Spieler 2:", font=['Arial', 14, 'bold'])
        self.__player_two_name_entry = Entry(self)
        self.__start_button = Button(self, text="Start Game", font=['Arial', 14, 'bold'])
        # Layouting
        self.__header_label.pack(side="top", expand=True, fill="both")
        self.__start_button.pack(side="bottom", expand=True, fill="x")
        self.__player_one_label.pack(side="top", expand=True)
        self.__player_one_name_entry.pack(side="top", expand=True)
        self.__player_two_label.pack(side="top", expand=True, )
        self.__player_two_name_entry.pack(side="top", expand=True)

    def get_first_player_name_input(self) -> str:
        return self.__player_one_name_entry.get()

    def get_second_player_name_input(self) -> str:
        return self.__player_two_name_entry.get()

    def on_start_button_clicked(self, command) -> None:
        self.__start_button.configure(command=command)
