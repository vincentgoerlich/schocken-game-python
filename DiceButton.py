from tkinter import Button

from IconCache import IconCache

"""
Tkinter Button extension to visually represent the dices and their rolled eyes.
"""


class DiceButton(Button):
    """
    Constructs a DiceButton object and therefore needs the root-tkinter component and access
    to the IconCache in order to update its visual representation of a die and its eyes.
    By default, a new object is not locked and has 0 eye count.
    """

    def __init__(self, root, icon_cache: IconCache):
        super(DiceButton, self).__init__(root)
        self.__image_cache = icon_cache
        self.__is_locked = False
        self.__eye_count = 0
        self.configure(command=self.on_click)

    def update_icon(self) -> None:
        """
        Updates the icon according to the eye count.
        :return: None
        """
        if self.__is_locked:
            corresponding_icon = self.__image_cache.get_locked_icon(self.__eye_count)
        else:
            corresponding_icon = self.__image_cache.get_unlocked_icon(self.__eye_count)
        self.configure(image=corresponding_icon)

    def set_eyes_and_update_icon(self, eye_count) -> None:
        """
        Sets the eye count and calls the update_icon() function.
        :param eye_count: the new eye count of the dice button
        :return: None
        """
        self.__eye_count = eye_count
        self.update_icon()

    def unlock_and_update_icon(self) -> None:
        """
        Sets the DiceButton to 'unlocked' and calls the update_icon() function.
        :return: None
        """
        self.__is_locked = False
        self.update_icon()

    def lock_and_update_icon(self):
        """
        Sets the DiceButton to 'locked' and calls the update_icon() function.
        Note: a freshly created DiceButton with __eye_count == 0 can not be locked.
        :return: None
        """
        if self.__eye_count != 0:
            self.__is_locked = True
            self.update_icon()

    def on_click(self) -> None:
        """
        When the DiceButton is clicked, it either locks or unlocks, dependent on its previous state.
        :return: None
        """
        if self.__is_locked:
            self.unlock_and_update_icon()
        else:
            self.lock_and_update_icon()

    def is_locked(self) -> bool:
        return self.__is_locked