import DiceInterpreter
from PlayerModel import PlayerModel


def translate_eyes_to_tokens(winner: PlayerModel) -> int:
    if DiceInterpreter.has_shock(winner):
        if DiceInterpreter.has_shock_out(winner):
            return 11
        else:
            return winner.get_sorted_eyes()[0]
    if DiceInterpreter.has_triplet(winner):
        return 3
    if DiceInterpreter.has_street(winner):
        return 2
    return 1


class TokenUtil:
    pass
