from PIL import Image, ImageTk

"""
Dedicated class for initializing and storing the image icons for this project.
"""

class IconCache:

    def __init__(self):
        dice_1_image = Image.open("./images/Dice_1.jpg")
        dice_2_image = Image.open("./images/Dice_2.jpg")
        dice_3_image = Image.open("./images/Dice_3.jpg")
        dice_4_image = Image.open("./images/Dice_4.jpg")
        dice_5_image = Image.open("./images/Dice_5.jpg")
        dice_6_image = Image.open("./images/Dice_6.jpg")
        token_image = Image.open("./images/beer.jpg")
        dice_back_ground_image = Image.open("./images/dice_background.jpg")

        dice_1_image_locked = Image.open("./images/Dice_1_locked.jpg")
        dice_2_image_locked = Image.open("./images/Dice_2_locked.jpg")
        dice_3_image_locked = Image.open("./images/Dice_3_locked.jpg")
        dice_4_image_locked = Image.open("./images/Dice_4_locked.jpg")
        dice_5_image_locked = Image.open("./images/Dice_5_locked.jpg")
        dice_6_image_locked = Image.open("./images/Dice_6_locked.jpg")
        roll_image = Image.open("./images/roll.jpg")
        not_rolled_yet_dice_image = Image.open("./images/not_rolled_yet_dice.jpg")
        self.real_dice_1_image = ImageTk.PhotoImage(dice_1_image)
        self.real_dice_2_image = ImageTk.PhotoImage(dice_2_image)
        self.real_dice_3_image = ImageTk.PhotoImage(dice_3_image)
        self.real_dice_4_image = ImageTk.PhotoImage(dice_4_image)
        self.real_dice_5_image = ImageTk.PhotoImage(dice_5_image)
        self.real_dice_6_image = ImageTk.PhotoImage(dice_6_image)
        self.real_dice_1_image_locked = ImageTk.PhotoImage(dice_1_image_locked)
        self.real_dice_2_image_locked = ImageTk.PhotoImage(dice_2_image_locked)
        self.real_dice_3_image_locked = ImageTk.PhotoImage(dice_3_image_locked)
        self.real_dice_4_image_locked = ImageTk.PhotoImage(dice_4_image_locked)
        self.real_dice_5_image_locked = ImageTk.PhotoImage(dice_5_image_locked)
        self.real_dice_6_image_locked = ImageTk.PhotoImage(dice_6_image_locked)
        self.roll_image = ImageTk.PhotoImage(roll_image)
        self.dice_background = ImageTk.PhotoImage(dice_back_ground_image)
        self.real_dice_not_rolled_yet_image = ImageTk.PhotoImage(not_rolled_yet_dice_image)
        self.real_token_image = ImageTk.PhotoImage(token_image)

    def get_unlocked_icon(self, eye_count) -> ImageTk.PhotoImage:
        if eye_count == 0:
            return self.real_dice_not_rolled_yet_image
        if eye_count == 1:
            return self.real_dice_1_image
        if eye_count == 2:
            return self.real_dice_2_image
        if eye_count == 3:
            return self.real_dice_3_image
        if eye_count == 4:
            return self.real_dice_4_image
        if eye_count == 5:
            return self.real_dice_5_image
        if eye_count == 6:
            return self.real_dice_6_image

    def get_locked_icon(self, eye_count) -> ImageTk.PhotoImage:
        if eye_count == 1:
            return self.real_dice_1_image_locked
        if eye_count == 2:
            return self.real_dice_2_image_locked
        if eye_count == 3:
            return self.real_dice_3_image_locked
        if eye_count == 4:
            return self.real_dice_4_image_locked
        if eye_count == 5:
            return self.real_dice_5_image_locked
        if eye_count == 6:
            return self.real_dice_6_image_locked

    def get_token_icon(self) -> ImageTk.PhotoImage:
        return self.real_token_image

